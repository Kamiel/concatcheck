.PHONY: all benchmarks build clean configure coverage docs init install repl run test

jobs = 9

all: init configure build docs test coverage benchmarks

benchmarks:
	stack build --bench --jobs $(jobs) \
	&& $$(stack path --dist-dir)/build/benchmarks/benchmarks -o $$(stack path --project-root)/benchmarks.html \
	&& xdg-open $$(stack path --project-root)/benchmarks.html

build:
	stack build --jobs $(jobs)

clean:
	stack clean
	if test -f stack.yaml; then rm -r stack.yaml; fi
	if test -d .hpc; then rm -r .hpc; fi
	if test -f benchmarks.html; then rm -r benchmarks.html; fi
	if test -f benchmarks.tix; then rm -r benchmarks.tix; fi

configure:
	cabal configure --enable-benchmarks --enable-tests --enable-coverage --enable-profiling --package-db=clear --package-db=global --package-db=$$(stack path --snapshot-pkg-db) --package-db=$$(stack path --local-pkg-db)
#  --builddir=$$(stack path --dist-dir)

coverage:
	stack exec -- hpc report --hpcdir=$$(stack path --dist-dir)/hpc/.hpc --per-module $$(find . -name tests.tix)

docs:
	stack haddock --no-haddock-deps \
	&& xdg-open $$(stack path --local-doc-root)/ConcatCheck-0.0.1/index.html

init:
	stack init --force --resolver lts-3.4
	stack solver --modify-stack-yaml

install:
	stack install

profile:
	stack build --library-profiling --executable-profiling --jobs $(jobs)

repl:
	stack ghci --jobs $(jobs)

run:
	stack exec ConcatCheck --jobs $(jobs)

test:
	stack build --test --coverage --jobs $(jobs)
# --coverage --ghc-options "--exclude=tests --exclude=examples --exclude=coverage --exclude=documentation --exclude=style --exclude=benchmarks"
