-- {-# OPTIONS_GHC -F -pgmF hspec-discover #-}

module Main (main) where

import Test.Tasty (defaultMain)
import Test.Tasty.Hspec

import qualified ConcatCheckSpec
-- HASKELETON: import qualified New.ModuleSpec

main :: IO ()
main = testSpec "Spec" spec >>= defaultMain

spec :: Spec
spec = do
    describe "ConcatCheck" ConcatCheckSpec.spec
    -- HASKELETON: describe "New.Module" New.ModuleSpec.spec
