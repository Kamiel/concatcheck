module ConcatCheckSpec (spec) where

import ConcatCheck
import Data.Sequence ( fromList )
import Data.ByteString.Char8 ( pack )

import Test.Hspec

spec :: Spec
spec = do
    describe "main" $
        it "returns the unit" $
            main `shouldReturn` ()

    let dict = ["world", "hello", "super", "hell"]
        p1 = "helloworld"
        p2 = "superman"
        p3 = "hellohello"
        p4 = "aquaman"
        p5 = "hell"
        p6 = "orion"
        dict2 = dict ++ [p6]

    describe "isConcatenation" $ do
        it ("with " ++ show dict ++ " and " ++ p1 ++ " should return True") $
           isConcatenation dict p1 `shouldBe` True
        it ("with " ++ show dict ++ " and " ++ p2 ++ " should return False") $
           isConcatenation dict p2 `shouldBe` False
        it ("with " ++ show dict ++ " and " ++ p3 ++ " should return True") $
           isConcatenation dict p3 `shouldBe` True
        it "should work with strings from dict that are prefix one to other" $
           isConcatenation dict2 (p5 ++ p6) `shouldBe` True
        it "should return False with empty string" $
           isConcatenation dict "" `shouldBe` False
        it "should return False with empty dictionary" $
           isConcatenation [] p5 `shouldBe` False
        it "should return True for isConcatenation [\"aaa\",\"aa\"] \"aaaa\"" $
           isConcatenation ["aaa", "aa"] "aaaa" `shouldBe` True

    describe "prefix" $ do
        it "should be lazy" $
           prefix (pack <$> fromList ["abc", "de", undefined]) (pack "abcde") `shouldBe` True
        it ("with " ++ show dict ++ " and " ++ show [p4] ++ " should return False") $
           prefix (pack <$> fromList dict) (pack p4) `shouldBe` False
