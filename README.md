## Algorithm description.
Trying to find prefix in key string from dictionary string list step by step.
On each step check is current string from dictionary already identical to string.
If yes — key string is matched to concatenation of strings from the dictionary.
Otherwise, in case it mismatch with all of the strings from dictionary,
key string is not a concatenation as well.
Finally, if current string from dictionary matched just part of string,
continue in the same way with rest part of string for all match invariants.

## Algorithmic complexity
Let *K* is length of key string, *M* dictionary elements count and *T* — maximum element length.
So algorithm comlexity is

    => O(isConcatenation') * O(match)
    => O(K) * O(O(fold) * traverse * O(prefix))
         where
           m = M
           p = e^e
           traverse = m * p

`p = e^e` — for worst alhorithm case only. Actually number of matches on each
iteration equal count of strings that may be prefix part of string. So
practically it will be not more than M.

    => O(K * O(fold) * M * P * T)

where *P* — maximum count of strings from dictionary that are prefix one to other simultaneously.
`fold` for `Seq` is *O(log(M))*.

     => O(K * log(M) * M * P * T)

So in general, according to description, algorithmic complexity is **O(log(N) * N)**

## Improvement
Further performance improvement by adding concurrency inefficient,
*see:* <http://stackoverflow.com/questions/32755353/data-sequence-seq-lazy-parallel-functor-instance>

# ConcatCheck

You are given a dictionary of strings and a key.  Check if the key is
a concatenation of strings from the dictionary.

Your solution must be expressed as a function

    isConcatenation :: [String] -> String -> Bool

Examples
========

If dictionary is the list ["world", "hello", "super", "hell"], then

    isConcatenation dictionary "helloworld" should return True
    isConcatenation dictionary "superman"   should return False
    isConcatenation dictionary "hellohello" should return True

The last example shows in particular that each dictionary string can
be used an arbitrary number of times.

You may assume that the input list of strings contains no duplicates.

General
=======

You may NOT use any third party packages other than 'base'.


Based on (with modifications) [haskeleton]: http://taylor.fausak.me/haskeleton/

``` sh
# Update Cabal's list of packages.
cabal update

# Initialize a sandbox and install the package's dependencies.
make init

# Configure if necessary.
make configure

# Build package and dependencies.
make build

# Install the binaries in a shared location.
make install

# Test package.
make test

# Benchmark package.
make benchmarks

# Run executable.
make run

# Start REPL.
make repl

# Generate documentation.
make docs

# Analyze coverage.
make coverage
```
