{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -funbox-strict-fields #-}

{-|
Module      : ConcatCheck
Description : Check if the key is a concatenation of strings from the dictionary.
Copyright   : (c) Pitomets Yuriy, 2015
License     : MIT
Maintainer  : pitometsu@gmail.com
Stability   : experimental
Portability : POSIX

== Algorithm description.
Trying to find prefix in key string from dictionary string list step by step.
On each step check is current string from dictionary already identical to string.
If yes — key string is matched to concatenation of strings from the dictionary.
Otherwise, in case it mismatch with all of the strings from dictionary,
key string is not a concatenation as well.
Finally, if current string from dictionary matched just part of string,
continue in the same way with rest part of string for all match invariants.

== Algorithmic complexity
Let /K/ is length of key string, /M/ dictionary elements count and /T/ — maximum element length.
So algorithm comlexity is

>   => O(isConcatenation') * O(match)
>   => O(K) * O(O(fold) * traverse * O(prefix))
>        where
>          m = M
>          p = e^e
>          traverse = m * p

@p = e^e@ — for worst alhorithm case only. Actually number of matches on each
iteration equal count of strings that may be prefix part of string. So
practically it will be not more than /M/.

>   => O(K * O(fold) * M * P * T)

where /P/ — maximum count of strings from dictionary that are prefix one to other simultaneously.
@fold@ for @Seq@ is /O(log(M))/.

>    => O(K * log(M) * M * P * T)

So in general, according to description, algorithmic complexity is __O(log(N) * N)__

== Improvement
Further performance improvement by adding concurrency inefficient,
/see:/ <http://stackoverflow.com/questions/32755353/data-sequence-seq-lazy-parallel-functor-instance>
-}
module ConcatCheck (module ConcatCheck) where

import Prelude hiding ( length, drop )
import Data.Sequence ( Seq, fromList )
import Data.ByteString ( ByteString, isPrefixOf, length, drop )
import Data.ByteString.Char8 ( pack )

-- | Check if key strings start with one of prefix strings from the dictionary.
--   Recursively continue with rest of string without matched part.
prefix       :: Seq ByteString -- ^ list of prefix strings
             -> ByteString     -- ^ rest of key string suffix
             -> Bool
{-# INLINE prefix #-}
prefix !k !r = or $ match r <$> k
  where
    match !s !p = let !pr = p `isPrefixOf` s
                  in  pr && (p == s || (prefix k $! drop (length p) s))

-- | Check if the key is a concatenation of any strings from the dictionary.
isConcatenation       :: [String] -- ^ dictionary of strings
                      -> String   -- ^ key string to check
                      -> Bool
isConcatenation !p !s = let !ps = pack <$> fromList p
                            !s' = pack s
                        in  prefix ps s'

-- | just placeholder
main :: IO ()
main = return ()
