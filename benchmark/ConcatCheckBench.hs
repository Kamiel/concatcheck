module ConcatCheckBench (benchmarks) where

import ConcatCheck

import Criterion

benchmarks :: [Benchmark]
benchmarks =
    [ bench "main" (nfIO main)
    , bgroup "isConcatenation" [ bench (show (length dict) ++ " keys, helloworld") $ nf (uncurry isConcatenation) (dict, "helloworld")
                               , bench (show (length dict) ++ " keys, superman") $ nf (uncurry isConcatenation) (dict, "superman")
                               , bench (show (length dict) ++ " keys, hellohello") $ nf (uncurry isConcatenation) (dict, "hellohello")
                               , bench (show (length longDict) ++ " keys, quuxcorgebarfoobazthud") $ nf (uncurry isConcatenation) (longDict, "quuxcorgebarfoobazthud")
                               , bench (show (length longDict) ++ " keys, quuxcorisgnotebarfoobazthud") $ nf (uncurry isConcatenation) (longDict, "quuxcorisgnotebarfoobazthud")
                               , bench (show (length names) ++ " keys, AmiasMasilEsauWaldoZachariasThorArchibaldArchie") $ nf (uncurry isConcatenation) (names, "AmiasMasilEsauWaldoZachariasThorArchibaldArchie")
                               , bench (show (length names) ++ " keys, Zacharias") $ nf (uncurry isConcatenation) (names, "Zacharias")
                               ]
    ]
  where
    dict = ["world", "hello", "super", "hell"]
    longDict = ["foo", "bar", "baz", "qux", "quux", "corge", "grault", "garply", "waldo", "fred", "plugh", "xyzzy", "thud"]
    names = ["Abner", "Alistair", "Aloysius", "Ambrose", "Amias", "Angus", "Archibald", "Archie", "Balthazar", "Barnaby", "Bartholomew", "Basil", "Benedict", "Bernard", "Boaz", "Boris", "Caesar", "Casper", "Caspian", "Cato", "Chester", "Clarence", "Claude", "Clifford", "Clyde", "Conan", "Constantine", "Cornelius", "Cosmo", "Dashiell", "Eamon", "Edmond", "Edmund", "Enoch", "Ephraim", "Esau", "Ewan", "Finnian", "Floyd", "Glen", "Glenn", "Gray", "Guy", "Harris", "Herbert", "Hercules", "Homer", "Horace", "Horatio", "Hubert", "Humphrey", "Ignatius", "Ira", "Jupiter", "Kermit", "Lazarus", "Leander", "Lev", "Linus", "Lloyd", "Lucius", "Luther", "Magnus", "Mahlon", "Mercury", "Monroe", "Montgomery", "Mordecai", "Mordechai", "Morris", "Nigel", "Norman", "Obadiah", "Oberon", "Octavio", "Octavius", "Odysseus", "Orson", "Osias", "Otis", "Ozias", "Peregrine", "Phineas", "Ralph", "Randolph", "Remus", "Robin", "Romulus", "Roscoe", "Rudolph", "Rufus", "Rupert", "Sampson", "Shephard", "Shepherd", "Sherman", "Stellan", "Sylvester", "Thor", "Ulysses", "Urban", "Valentine", "Vladimir", "Waldo", "Wallace", "Wolfgang", "Woodrow", "Zacharias"]
